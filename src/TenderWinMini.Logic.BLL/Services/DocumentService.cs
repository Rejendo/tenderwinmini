﻿using TenderWinMini.Data.Contracts;
using TenderWinMini.Logic.BLL.Services.Base;
using TenderWinMini.Logic.Contracts.Services;

namespace TenderWinMini.Logic.BLL.Services {
    public class DocumentService : BaseService, IDocumentService {
        public DocumentService(IUnitOfWork unitOfWork) : base(unitOfWork) {
        }
    }
}
