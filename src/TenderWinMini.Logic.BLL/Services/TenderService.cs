﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TenderWinMini.Data.Contracts;
using TenderWinMini.Logic.BLL.Services.Base;
using TenderWinMini.Logic.Contracts.Dtos;
using TenderWinMini.Logic.Contracts.Services;

namespace TenderWinMini.Logic.BLL.Services {
    public class TenderService : BaseService, ITenderService {
        private const int TENDER_PER_PAGE = 50;
        public TenderService(IUnitOfWork unitOfWork) : base(unitOfWork) {
        }

        public async Task<IEnumerable<BriefTenderDto>> GetTenderList(int page = 1, string search = "", decimal? priceFrom = null, decimal? priceTo = null,
            DateTime? publicationFrom = null, DateTime? publicationTo = null) {
            var items = await _unitOfWork.TenderRepository.All
                .Where(a => !a.IsArchived
                && (string.IsNullOrEmpty(search) || a.Name.Contains(search))
                && (!priceFrom.HasValue || a.Prise > priceFrom)
                && (!priceTo.HasValue || a.Prise < priceTo)
                && (!publicationFrom.HasValue || a.PublicatedDate > publicationFrom)
                && (!publicationTo.HasValue || a.PublicatedDate < publicationTo))
                .OrderBy(x => x.CompletedDate)
                .Skip(TENDER_PER_PAGE * (page - 1))
                .Take(TENDER_PER_PAGE)
                .ToListAsync();

            return Mapper.Map<IEnumerable<BriefTenderDto>>(items);
        }

        public async Task<TenderDto> GetTenderById(Guid id) {
            var item = await _unitOfWork.TenderRepository.GetByIdAsync(id);

            return Mapper.Map<TenderDto>(item);
        }

        public async Task<IEnumerable<DocumentDto>> GetDocumentListByTenderId(Guid id) {
            var item = (await _unitOfWork.TenderRepository.GetByIdAsync(id)).Documents.ToList();

            return Mapper.Map<IEnumerable<DocumentDto>>(item);
        }

        public async Task<IEnumerable<EventDto>> GetEventListByTenderId(Guid id) {
            var item = (await _unitOfWork.TenderRepository.GetByIdAsync(id)).Events.ToList();

            return Mapper.Map<IEnumerable<EventDto>>(item);
        }
    }
}
