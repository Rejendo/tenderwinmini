﻿using System;
using TenderWinMini.Data.Contracts;

namespace TenderWinMini.Logic.BLL.Services.Base {
    public class BaseService {
        protected readonly IUnitOfWork _unitOfWork;

        public BaseService(IUnitOfWork unitOfWork) {
            if (unitOfWork == null) {
                throw new ArgumentNullException("unitOfWork");
            }

            _unitOfWork = unitOfWork;
        }
    }
}
