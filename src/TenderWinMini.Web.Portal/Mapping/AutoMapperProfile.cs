﻿using AutoMapper;
using System;
using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Logic.Contracts.Dtos;
using TenderWinMini.Web.Portal.Models;

namespace TenderWinMini.Web.Portal.Mapping {
    public class AutoMapperProfile : Profile {
        public AutoMapperProfile() {
            // View <-> BLL
            CreateMap<BriefTenderDto, BriefTenderViewModel>();

            // BLL <-> DAL
            CreateMap<Tender, BriefTenderDto>()
                .ForMember(dest => dest.DaysLeft, opt => opt.MapFrom(src => (src.CompletedDate - DateTime.Now).Days));
            CreateMap<Tender, TenderDto>();
        }
    }
}