﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TenderWinMini.Logic.Contracts.Services;
using TenderWinMini.Web.Portal.Controllers.Base;
using TenderWinMini.Web.Portal.Models;

namespace TenderWinMini.Web.Portal.Controllers {
    public class TenderController : BaseController {
        private readonly ITenderService _tenderService;
        public TenderController(ITenderService tenderService) {
            _tenderService = tenderService;
        }

        public async Task<ActionResult> GetTenderList(int page = 1, string search = "", decimal? priceFrom = null, decimal? priceTo = null,
            DateTime? publicationFrom = null, DateTime? publicationTo = null) {
            var model = Mapper.Map<IEnumerable<BriefTenderViewModel>>(await _tenderService.GetTenderList(page, search, priceFrom, priceTo, publicationFrom, publicationTo));

            if (Request.IsAjaxRequest()) {
                return PartialView("_Items", model);
            }
            return View(model);
        }

        public async Task<ActionResult> GetTenderById(Guid id) {
            var model = Mapper.Map<TenderViewModel>(await _tenderService.GetTenderById(id));

            return PartialView(model);
        }

        public async Task<ActionResult> GetDocumentListByTenderId(Guid id) {
            var model = Mapper.Map<IEnumerable<DocumentViewModel>>(await _tenderService.GetDocumentListByTenderId(id));

            return PartialView(model);
        }

        public async Task<ActionResult> GetEventListByTenderId(Guid id) {
            var model = Mapper.Map<IEnumerable<EventViewModel>>(await _tenderService.GetEventListByTenderId(id));

            return PartialView(model);
        }
    }
}