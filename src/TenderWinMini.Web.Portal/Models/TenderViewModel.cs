﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TenderWinMini.Data.Contracts.Models.Entities;

namespace TenderWinMini.Web.Portal.Models {
    public class TenderViewModel {
        public string Name { get; set; }
        public Decimal Prise { get; set; }
        public DateTime PublicatedDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryAddress { get; set; }
    }
}