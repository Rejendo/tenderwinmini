﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TenderWinMini.Web.Portal.Models {
    public class BriefTenderViewModel {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Decimal Prise { get; set; }
        public int DaysLeft { get; set; }
    }
}