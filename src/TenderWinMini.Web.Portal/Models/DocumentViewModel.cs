﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TenderWinMini.Web.Portal.Models {
    public class DocumentViewModel {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}