﻿using Autofac;
using Autofac.Integration.Mvc;
using System.Data.Entity;
using System.Web.Mvc;
using TenderWinMini.Data.Contracts;
using TenderWinMini.Data.DB;
using TenderWinMini.Data.DB.Context;
using TenderWinMini.Logic.BLL.Services;
using TenderWinMini.Logic.Contracts.Services;

namespace TenderWinMini.Web.Portal.App_Start {
    public class AutofacConfig {
        public static void ConfigureContainer() {
            var builder = new ContainerBuilder();

            // DAL
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<TenderDbContext>().As<DbContext>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().WithParameter("connectionString", "TenderDB");

            // BLL
            builder.RegisterType<TenderService>().As<ITenderService>();
            builder.RegisterType<EventService>().As<IEventService>();
            builder.RegisterType<DocumentService>().As<IDocumentService>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}