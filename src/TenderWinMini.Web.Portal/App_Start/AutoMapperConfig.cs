﻿using AutoMapper;
using TenderWinMini.Web.Portal.Mapping;

namespace TenderWinMini.Web.Portal.App_Start {
    public static class AutoMapperConfig {
        public static void Initialize() {
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapperProfile()));
        }
    }
}