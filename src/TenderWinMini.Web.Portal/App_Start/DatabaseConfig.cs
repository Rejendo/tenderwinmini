﻿using System.Data.Entity;
using TenderWinMini.Data.DB.Context;

namespace TenderWinMini.Web.Portal.App_Start {
    public static class DatabaseConfig {
        public static void Initialize() {
            Database.SetInitializer(new TenderDbContextInitializer());
        }
    }
}