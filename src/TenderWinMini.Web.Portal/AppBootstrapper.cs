﻿using TenderWinMini.Web.Portal.App_Start;

namespace TenderWinMini.Web.Portal {
    public static class AppBootstrapper {

        public static void Init() {
            DatabaseConfig.Initialize();
            AutofacConfig.ConfigureContainer();
            AutoMapperConfig.Initialize();
        }
    }
}