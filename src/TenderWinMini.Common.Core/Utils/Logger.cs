﻿using NLog;
using System;

namespace TenderWinMini.Common.Core.Utils {
    public static class Logger {
        private static NLog.Logger _logger = LogManager.GetLogger("TenderLogger");

        public static void Error(Exception ex, string message, params object[] args) {
            Error(ex, string.Format(message, args));
        }
        public static void Error(Exception ex, string message) {
            _logger.Error(ex, message);
        }
        public static void Error(Exception ex) {
            _logger.Error(ex);
        }
        public static void Error(string message) {
            _logger.Error(message);
        }
        public static void Debug(string message, params object[] args) {
            Debug(String.Format(message, args));
        }
        public static void Debug(string message) {
            _logger.Debug(message);
        }
        public static void Info(string message, params object[] args) {
            Info(String.Format(message, args));
        }
        public static void Info(string message) {
            _logger.Info(message);
        }
    }
}
