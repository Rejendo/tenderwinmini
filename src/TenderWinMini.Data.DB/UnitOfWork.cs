﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TenderWinMini.Data.Contracts;
using TenderWinMini.Data.Contracts.Repositories;
using TenderWinMini.Data.DB.Context;
using TenderWinMini.Data.DB.Repositories;

namespace TenderWinMini.Data.DB {
    public class UnitOfWork : IUnitOfWork {
        private readonly TenderDbContext _dbContext;

        private IDocumentRepository _documentRepository;
        private IEventRepository _eventRepository;
        private ITenderRepository _tenderRepository;

        public UnitOfWork(string connectionString) {
            if (connectionString == null) {
                throw new ArgumentNullException("connectionString");
            }

            _dbContext = new TenderDbContext();
        }

        public void SaveChanges() {
            _dbContext.SaveChanges();
        }
        public async Task SaveChangesAsync() {
            await _dbContext.SaveChangesAsync();
        }

        public void Rollback() {
            var changedEntries = this._dbContext.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries) {
                switch (entry.State) {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public IDocumentRepository DocumentRepository {
            get { return _documentRepository ?? (_documentRepository = new DocumentRepository(_dbContext)); }
        }

        public IEventRepository EventRepository {
            get { return _eventRepository ?? (_eventRepository = new EventRepository(_dbContext)); }
        }

        public ITenderRepository TenderRepository {
            get { return _tenderRepository ?? (_tenderRepository = new TenderRepository(_dbContext)); }
        }
    }
}
