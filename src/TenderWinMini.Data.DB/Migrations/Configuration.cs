namespace TenderWinMini.Data.DB.Migrations {
    using System.Data.Entity.Migrations;

    public class Configuration : DbMigrationsConfiguration<TenderWinMini.Data.DB.Context.TenderDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TenderWinMini.Data.DB.Context.TenderDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
