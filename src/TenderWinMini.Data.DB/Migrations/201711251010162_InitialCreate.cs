namespace TenderWinMini.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tenders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Prise = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PublicatedDate = c.DateTime(nullable: false),
                        CompletedDate = c.DateTime(nullable: false),
                        DeliveryTime = c.String(),
                        DeliveryAddress = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TenderId = c.Guid(nullable: false),
                        Name = c.String(),
                        Url = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenders", t => t.TenderId, cascadeDelete: true)
                .Index(t => t.TenderId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TenderId = c.Guid(nullable: false),
                        Text = c.String(),
                        EvetnDate = c.DateTime(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenders", t => t.TenderId, cascadeDelete: true)
                .Index(t => t.TenderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "TenderId", "dbo.Tenders");
            DropForeignKey("dbo.Documents", "TenderId", "dbo.Tenders");
            DropIndex("dbo.Events", new[] { "TenderId" });
            DropIndex("dbo.Documents", new[] { "TenderId" });
            DropTable("dbo.Events");
            DropTable("dbo.Documents");
            DropTable("dbo.Tenders");
        }
    }
}
