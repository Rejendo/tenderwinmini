﻿using System.Data.Entity.ModelConfiguration;
using TenderWinMini.Data.Contracts.Models.Entities;

namespace TenderWinMini.Data.DB.Maps {
    public class TenderMap : EntityTypeConfiguration<Tender> {
        public TenderMap() {
            this.HasKey(t => new { t.Id });
        }
    }
}
