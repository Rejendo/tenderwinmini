﻿using System.Data.Entity.ModelConfiguration;
using TenderWinMini.Data.Contracts.Models.Entities;

namespace TenderWinMini.Data.DB.Maps {
    public class EventMap : EntityTypeConfiguration<Event> {
        public EventMap() {
            this.HasKey(e => new { e.Id })
                .HasRequired(e => e.Tender)
                .WithMany(e => e.Events)
                .HasForeignKey(e => e.TenderId);
        }
    }
}
