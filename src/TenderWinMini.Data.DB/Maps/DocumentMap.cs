﻿using System.Data.Entity.ModelConfiguration;
using TenderWinMini.Data.Contracts.Models.Entities;

namespace TenderWinMini.Data.DB.Maps {
    public class DocumentMap : EntityTypeConfiguration<Document> {
        public DocumentMap() {
            this.HasKey(d => new { d.Id })
                .HasRequired(d => d.Tender)
                .WithMany(d => d.Documents)
                .HasForeignKey(d => d.TenderId);
        }
    }
}
