﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderWinMini.Data.DB.Migrations;

namespace TenderWinMini.Data.DB.Context {
    public class TenderDbContextInitializer : MigrateDatabaseToLatestVersion<TenderDbContext, Configuration> {
    }
}
