﻿using System.Data.Entity;
using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Data.DB.Maps;

namespace TenderWinMini.Data.DB.Context {
    public class TenderDbContext : DbContext {
        public TenderDbContext()
            : base("TenderDB") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new TenderMap());
            modelBuilder.Configurations.Add(new EventMap());
            modelBuilder.Configurations.Add(new DocumentMap());

            modelBuilder.Entity<Tender>().ToTable("Tenders");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<Document>().ToTable("Documents");
        }

        public static TenderDbContext Create() {
            return new TenderDbContext();
        }
    }
}
