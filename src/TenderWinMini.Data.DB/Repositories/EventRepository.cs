﻿using TenderWinMini.Data.DB.Repositories.Base;
using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Data.Contracts.Repositories;
using TenderWinMini.Data.DB.Context;

namespace TenderWinMini.Data.DB.Repositories {
    class EventRepository : BaseRepository<TenderDbContext, Event>, IEventRepository {
        public EventRepository(TenderDbContext dbContext) : base(dbContext) {
        }
    }
}
