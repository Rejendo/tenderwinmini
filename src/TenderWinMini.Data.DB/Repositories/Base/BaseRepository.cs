﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TenderWinMini.Data.Contracts.Models.Entities.Base;
using TenderWinMini.Data.Contracts.Repositories.Base;

namespace TenderWinMini.Data.DB.Repositories.Base {
    public class BaseRepository<TContext, TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext {
        public BaseRepository(TContext dbContext) {
            if (dbContext == null) {
                throw new ArgumentNullException("dbContext");
            }
            DbContext = dbContext;
            DbSet = DbContext.Set<TEntity>();
        }
        protected TContext DbContext { get; private set; }
        protected DbSet<TEntity> DbSet { get; set; }

        public IQueryable<TEntity> All {
            get { return DbSet; }
        }

        public IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties) {
            IQueryable<TEntity> query = All;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public virtual void Update(TEntity entity) {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached) {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public virtual void Delete(TEntity entity) {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted) {
                dbEntityEntry.State = EntityState.Deleted;
            } else {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
            SaveChanges();
        }

        public void SaveChanges() {
            DbContext.SaveChanges();
        }

        public Task<TEntity> GetByIdAsync(Guid id) {
            return DbSet.FindAsync(id);
        }

        public bool Exists(Guid id) {
            return DbSet.Find(id) != null;
        }

        public virtual async Task DeleteAsync(Guid id) {
            var entity = await GetByIdAsync(id);
            if (entity == null) return;
            Delete(entity);
            SaveChanges();
        }

        public virtual void Add(TEntity entity) {
            var baseEntity = entity as BaseEntity;
            if (baseEntity != null && baseEntity.Id == Guid.Empty) {
                baseEntity.Id = Guid.NewGuid();
            }
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached) {
                dbEntityEntry.State = EntityState.Added;
            } else {
                DbSet.Add(entity);
            }
            SaveChanges();
        }

        public virtual void Archive(TEntity entity) {
            var archivable = entity as BaseEntity;
            if (archivable == null) {
                throw new InvalidCastException(string.Format("{0} is not archivable", entity.GetType()));
            }

            archivable.IsArchived = true;
            Update(entity);
        }

        public virtual async Task ArchiveAsync(Guid id) {
            var entity = await GetByIdAsync(id);
            if (entity == null) return;

            Archive(entity);
        }

        public virtual void Unarchive(TEntity entity) {
            var archivable = entity as BaseEntity;
            if (archivable == null) {
                throw new InvalidCastException(string.Format("{0} is not archivable", entity.GetType()));
            }

            archivable.IsArchived = false;
            Update(entity);
        }

        public virtual async Task UnarchiveAsync(Guid id) {
            var entity = await GetByIdAsync(id);
            if (entity == null) return;

            Unarchive(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities) {
            DbSet.AddRange(entities);
            SaveChanges();
        }
    }
}
