﻿using TenderWinMini.Data.DB.Repositories.Base;
using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Data.Contracts.Repositories;
using TenderWinMini.Data.DB.Context;

namespace TenderWinMini.Data.DB.Repositories {
    class TenderRepository : BaseRepository<TenderDbContext, Tender>, ITenderRepository {
        public TenderRepository(TenderDbContext dbContext) : base(dbContext) {
        }
    }
}
