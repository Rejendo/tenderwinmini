﻿using TenderWinMini.Data.DB.Repositories.Base;
using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Data.Contracts.Repositories;
using TenderWinMini.Data.DB.Context;

namespace TenderWinMini.Data.DB.Repositories {
    public class DocumentRepository : BaseRepository<TenderDbContext, Document>, IDocumentRepository {
        public DocumentRepository(TenderDbContext dbContext) : base(dbContext) {
        }
    }
}
