﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TenderWinMini.Logic.Contracts.Dtos;

namespace TenderWinMini.Logic.Contracts.Services {
    public interface ITenderService {
        Task<IEnumerable<BriefTenderDto>> GetTenderList(int page, string search, decimal? priceFrom, decimal? priceTo,
            DateTime? publicationFrom, DateTime? publicationTo);
        Task<TenderDto> GetTenderById(Guid id);
        Task<IEnumerable<DocumentDto>> GetDocumentListByTenderId(Guid id);
        Task<IEnumerable<EventDto>> GetEventListByTenderId(Guid id);
    }
}
