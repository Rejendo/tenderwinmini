﻿namespace TenderWinMini.Logic.Contracts.Dtos {
    public class DocumentDto {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
