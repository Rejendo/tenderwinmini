﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderWinMini.Data.Contracts.Models.Entities;

namespace TenderWinMini.Logic.Contracts.Dtos {
    public class TenderDto {
        public string Name { get; set; }
        public Decimal Prise { get; set; }
        public DateTime PublicatedDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryAddress { get; set; }
    }
}
