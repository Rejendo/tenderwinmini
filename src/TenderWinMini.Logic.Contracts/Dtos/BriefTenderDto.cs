﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderWinMini.Logic.Contracts.Dtos {
    public class BriefTenderDto {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Decimal Prise { get; set; }
        public int DaysLeft { get; set; }
    }
}
