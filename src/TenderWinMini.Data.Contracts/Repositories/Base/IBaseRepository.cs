﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TenderWinMini.Data.Contracts.Repositories.Base {
    public interface IBaseRepository<TEntity> {
        IQueryable<TEntity> All { get; }
        IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        Task DeleteAsync(Guid id);
        void SaveChanges();
        Task<TEntity> GetByIdAsync(Guid id);
        void Archive(TEntity entity);
        Task ArchiveAsync(Guid id);
        void Unarchive(TEntity entity);
        Task UnarchiveAsync(Guid id);
        void AddRange(IEnumerable<TEntity> entities);
    }
}
