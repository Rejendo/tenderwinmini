﻿using TenderWinMini.Data.Contracts.Models.Entities;
using TenderWinMini.Data.Contracts.Repositories.Base;

namespace TenderWinMini.Data.Contracts.Repositories {
    public interface ITenderRepository : IBaseRepository<Tender> {
    }
}
