﻿using System.Threading.Tasks;
using TenderWinMini.Data.Contracts.Repositories;

namespace TenderWinMini.Data.Contracts {
    public interface IUnitOfWork {
        void SaveChanges();
        Task SaveChangesAsync();
        void Rollback();
        IDocumentRepository DocumentRepository { get; }
        IEventRepository EventRepository { get; }
        ITenderRepository TenderRepository { get; }
    }
}
