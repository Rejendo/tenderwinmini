﻿using System;
using TenderWinMini.Data.Contracts.Models.Entities.Base;

namespace TenderWinMini.Data.Contracts.Models.Entities {
    public class Document : BaseEntity {
        public Guid? TenderId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public Tender Tender { get; set; }
    }
}
