﻿using System;
using System.Collections.Generic;
using TenderWinMini.Data.Contracts.Models.Entities.Base;

namespace TenderWinMini.Data.Contracts.Models.Entities {
    public class Tender : BaseEntity {
        public string Name { get; set; }
        public Decimal Prise { get; set; }
        public DateTime PublicatedDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryAddress { get; set; }
        
        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
