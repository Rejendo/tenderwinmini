﻿using System;
using TenderWinMini.Data.Contracts.Models.Entities.Base;

namespace TenderWinMini.Data.Contracts.Models.Entities {
    public class Event : BaseEntity {
        public Guid? TenderId { get; set; }
        public string Text { get; set; }
        public DateTime EvetnDate { get; set; }
        public Tender Tender { get; set; }
    }
}
