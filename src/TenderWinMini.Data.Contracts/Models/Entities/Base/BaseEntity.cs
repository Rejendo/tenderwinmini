﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TenderWinMini.Data.Contracts.Models.Entities.Base {
    public class BaseEntity {
        public BaseEntity() {
            Id = Guid.NewGuid();
            IsArchived = false;
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        public bool IsArchived { get; set; }
    }
}
